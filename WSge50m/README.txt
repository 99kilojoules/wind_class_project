Folder: WSge50m
2016 03 22
Created by G Scott for Julian Quick
Contains data files and graphics for 43 sites where we have a 50m WS measurement and at least
one more WS at > 50m.
See ../power.R for code that created these files.
