import operator
import seaborn as sns
from scipy.stats import norm
from matplotlib import pyplot as plt
from scipy import stats
import pandas as pd
import numpy as np
import xgboost as xgb
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import normalize

# Pick up data from parser
#dat = pd.read_csv('returned_data.csv')
#for d in dat.columns:
#    dat[d][dat[d] == np.nan] = np.nan
#    dat[d][dat[d] == 9999] = np.nan

# Get data
dat = pd.read_csv('./Site052WS.txt', sep=r'\s+', index_col=False)
dat.Index = dat.Index.map(pd.to_datetime)
dat['day'] = dat.Index.map(lambda x: x.day)
dat['month'] = dat.Index.map(lambda x: x.month)
dat['year'] = dat.Index.map(lambda x: x.year)
dat.drop('Index',axis=1,inplace=True)
dat.drop('time',axis=1,inplace=True)


parsed_dat = pd.DataFrame({'day':[], 'month':[], 'year':[], 'average speed at 50m':[], 'average speed at 75m':[]})
for year in np.unique(dat.year):
    for month in np.unique(dat.month): 
        for day in np.unique(dat.day):
            subdat = dat[ np.logical_and(dat.day==day, np.logical_and(dat.year==year, dat.month==month))]
            w50m = np.nanmean([np.nanmean(subdat.ix[:,0]), np.nanmean(subdat.ix[:,1])]) * 0.44704 # convert from mph to mps
            w75m = np.nanmean([np.nanmean(subdat.ix[:,2]), np.nanmean(subdat.ix[:,3])]) * 0.44704 # convert from mph to mps
            parsed_dat = parsed_dat.append({'day':day, 'month':month, 'year':year, 'average speed at 50m':w50m, 'average speed at 75m':w75m}, ignore_index=True)

# remove NAN
parsed_dat = parsed_dat[ np.logical_not(np.isnan(parsed_dat['average speed at 50m']))]

# Parse reference data
refdat = pd.read_csv('reference_Isabella.csv', index_col = False)
refdat.DATE = [pd.to_datetime(str(s)) for s in refdat.DATE]
refdat["year"] = refdat.DATE.map(lambda x: x.year)
refdat["month"] = refdat.DATE.map(lambda x: x.month)
refdat["day"] = refdat.DATE.map(lambda x: x.day)

# Drop unwanted fields
for badfield in ["DATE", "STATION", "STATION_NAME", "ELEVATION", "LATITUDE", "LONGITUDE"]:
    refdat.drop(badfield, axis=1, inplace=True)
refdat = refdat.convert_objects(convert_numeric=True)
df = pd.DataFrame()
df["year"] = refdat["year"]
df["month"] = refdat["month"]
df["day"] = refdat["day"]
for i in range(len(refdat.columns)/5):
    vals = refdat[refdat.columns[i*5]]
    vals[vals==np.nan] = np.nan
    vals[vals==9999] = np.nan
    df[ refdat.columns[i*5]] = vals

# Merge nrel data with reference data
train = pd.merge( parsed_dat, df, how="left", on = ["year", "month", "day"])
test = pd.merge( parsed_dat, df, how="right", on = ["year", "month", "day"])

# Convert categrical fields to numerical values
le = LabelEncoder()
for dfram in [train, test]:
    for feature in dfram.columns:
        if not np.isreal(dfram[feature][0]):
            dfram[feature] = le.fit_transform(dfram[feature])
        dfram[feature][dfram[feature]==-9999] = np.nan
        dfram[feature][dfram[feature]==9999] = np.nan
        dfram[feature][np.isnan(dfram[feature])] = np.nan

    # Unwanted data
    dfram.drop("SNWD", axis=1, inplace=True)
    dfram.drop("TSUN", axis=1, inplace=True)
    dfram = dfram.convert_objects( convert_numeric=True)
    
# NAN in target
train.drop(train.index[np.isnan(train['average speed at 75m'])], inplace=True)
train = train.reset_index(drop=True)

# take sample
rows = np.random.choice(train.index.values, 500)
train_known = train.ix[train.index[rows]]
train.drop(rows, inplace = True)

# train
target1 = u'average speed at 50m'
target2 = u'average speed at 75m'
Dtrain1 = xgb.DMatrix(train.drop('day',axis=1).drop('month',axis=1).drop('year',axis=1).drop(u'average speed at 50m',axis=1).drop(u'average speed at 75m',axis=1), train[target1], missing=np.nan)
Dtrain2 = xgb.DMatrix(train.drop('day',axis=1).drop('month',axis=1).drop('year',axis=1).drop(u'average speed at 50m',axis=1).drop(u'average speed at 75m',axis=1), train[target2], missing=np.nan)
Dtest = xgb.DMatrix(train_known.drop('day',axis=1).drop('month',axis=1).drop('year',axis=1).drop(u'average speed at 50m',axis=1).drop(u'average speed at 75m',axis=1), missing=np.nan)
modl1 = xgb.train({'subsample':0.7},Dtrain1, 6 )# 600)
modl2 = xgb.train({'subsample':0.7},Dtrain2, 6 )# 600)
pred1 = modl1.predict(Dtest)
pred2 = modl2.predict(Dtest)
print 'err1 is ',mean_squared_error(train_known[target1], pred1)
print 'err2 is ',mean_squared_error(train_known[target2], pred1)
    
# Predict
DT = xgb.DMatrix( test.drop('day',axis=1).drop('month',axis=1).drop('year',axis=1).drop(u'average speed at 50m',axis=1).drop(u'average speed at 75m',axis=1), missing=np.nan)
test['pred'+target1] = modl1.predict(DT)
test['pred'+target2] = modl2.predict(DT)
plt.hist(test['pred'+target1])
plt.savefig("finalHist"+target1+".pdf")
    
# parse features
def ceate_feature_map(features):
    outfile = open('xgb.fmap', 'w')
    i = 0
    for feat in features:
        outfile.write('{0}\t{1}\tq\n'.format(i, feat))
        i = i + 1
    outfile.close()
features = train.drop('day',axis=1).drop('month',axis=1).drop('year',axis=1).drop(u'average speed at 50m',axis=1).drop(u'average speed at 75m',axis=1).columns
ceate_feature_map(features)
importance = modl1.get_fscore(fmap='xgb.fmap')
importance = sorted(importance.items(), key=operator.itemgetter(1))
df = pd.DataFrame(importance, columns=['feature', 'fscore'])
df['fscore'] = df['fscore'] / df['fscore'].sum()

# Plot features
plt.figure(figsize=(20,10))
df.plot(kind='barh', x='feature', y='fscore', legend=False)
plt.title('XGBoost Feature Importance')
plt.xlabel('relative importance')
plt.savefig(target1+'relative_importance_for_xgb.pdf')
plt.close()

# compare methods - by points
plt.figure(figsize=(20,10))
plt.xlim([0,40])
cols = ['yellow', 'pink']
train_known['pred'] = pred1
plt.scatter(train_known.AWND, train_known['pred'], label='xgboost forecast', c='blue', s=50)
plt.scatter(train_known.AWND, train_known[target1], label='actual', c='green', marker = '^', s=50)
plt.legend()
labels = ['p{0}'.format(j+1) for j in range(train_known.shape[0])]
for df, c in zip([train_known.pred, train_known[target1]], cols):
    for lab, x, y in zip(labels, train_known.AWND.values, df.values):
        plt.annotate(lab, xy = (x,y),textcoords = 'offset points', ha = 'right', va = 'bottom',
        bbox = dict(boxstyle = 'round,pad=0.5', fc = c, alpha = 0.3),
        arrowprops = dict(arrowstyle = '->', connectionstyle = 'arc3,rad=0'))
plt.savefig(target1+'modelComparrisson.pdf')
plt.close()

# compare methods - by distributions
plt.close()
plt.figure(figsize=(20,10))
shape, _, scale = stats.weibull_min.fit([s for s in train_known.pred if not np.isnan(s)],loc=0)
plt.hist(train_known[target1].values, alpha=0.5, label="known, shp scl = "+', '.join(str(s) for s in [shape, scale]))
shape, _, scale = stats.weibull_min.fit([s for s in train_known[target1] if not np.isnan(s)],loc=0)
plt.hist(train_known.pred.values, alpha=0.5, label="predicted shp scl = "+', '.join(str(s) for s in [shape, scale]))
plt.legend()
plt.savefig(target1+'comp.pdf')
plt.close()

# annual averages
means = {}
means[target1] = []
means[target2] = []
shears = []
for year in np.unique( train.year):
    strain = train[train.year==year]
    means[target1].append( np.mean(strain[target1]))
    means[target2].append( np.mean(strain[target2]))
    shears.append( np.mean( np.log( strain[target2] / strain[target1]) / np.log(75./50)))

# plot shear coefficients
sns.distplot(shears)
mu, std = norm.fit(shears)
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2, label='normal distribution')
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)
plt.savefig('shear_pdf.pdf')
plt.clf()

# plot average speeds
sns.distplot(means[target1])
mu, std = norm.fit(means[target1])
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2, label='normal distribution')
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)
plt.savefig('avg_speed_at_50m.pdf')
plt.clf()

sns.distplot(means[target2])
mu, std = norm.fit(means[target2])
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2, label='normal distribution')
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)
plt.savefig('avg_speed_at_75_m.pdf')
plt.close()
