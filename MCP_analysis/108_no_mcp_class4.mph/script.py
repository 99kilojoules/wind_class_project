import operator
import seaborn as sns
from scipy.stats import norm
from matplotlib import pyplot as plt
from scipy import stats
import pandas as pd
import numpy as np
import xgboost as xgb
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import normalize

# Get data
dat = pd.read_csv('./Site108WS.txt', sep=r'\s+', index_col=False)
dat.Index = dat.Index.map(pd.to_datetime)
dat['day'] = dat.Index.map(lambda x: x.day)
dat['month'] = dat.Index.map(lambda x: x.month)
dat['year'] = dat.Index.map(lambda x: x.year)
dat.drop('Index',axis=1,inplace=True)
dat.drop('time',axis=1,inplace=True)

dat['mean50m'] = pd.DataFrame({'a':dat.ix[:,0].values, 'b':dat.ix[:,1].values}).max(1) * 0.44704 # MPH to MPS
dat['mean70m'] = pd.DataFrame({'a':dat.ix[:,2].values, 'b':dat.ix[:,3].values}).max(1) * 0.44704 # MPH to MPS

parsed_dat = pd.DataFrame({'day':[], 'month':[], 'year':[], 'average speed at 50m':[], 'average speed at 70m':[]})
for year in np.unique(dat.year)[0:-1]:
    for month in np.unique(dat.month): 
        for day in np.unique(dat.day):
            subdat = dat[ np.logical_and(dat.day==day, np.logical_and(dat.year==year, dat.month==month))]
            w50m = np.nanmean(subdat['mean50m'])
            w70m = np.nanmean(subdat['mean70m'])
            parsed_dat = parsed_dat.append({'day':day, 'month':month, 'year':year, 'average speed at 50m':w50m, 'average speed at 70m':w70m}, ignore_index=True)

target1 = 'average speed at 50m'
target2 = 'average speed at 70m'
# annual averages
means = {}
means[target1] = []
means[target2] = []
shears = []
for year in np.unique( parsed_dat.year):
    strain = parsed_dat[parsed_dat.year==year]
    means[target1].append( np.mean(strain[target1]))
    means[target2].append( np.mean(strain[target2]))
    shears.append( np.mean( np.log( np.ma.masked_invalid( strain[target2] / strain[target1])) / np.log(70./50)))

# plot shear coefficients
sns.distplot(shears)
mu, std = norm.fit(shears)
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2, label='normal distribution')
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)
plt.savefig('shear_pdf.pdf')
plt.clf()

# plot average speeds
sns.distplot(means[target1])
mu, std = norm.fit(means[target1])
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2, label='normal distribution')
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)
plt.savefig('avg_speed_at_50m.pdf')
plt.clf()

sns.distplot(means[target2])
mu, std = norm.fit(means[target2])
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2, label='normal distribution')
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)
plt.savefig('avg_speed_at_70_m.pdf')
plt.close()
