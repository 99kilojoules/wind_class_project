import operator
import seaborn as sns
from scipy.stats import norm
from matplotlib import pyplot as plt
from scipy import stats
import pandas as pd
import numpy as np
import xgboost as xgb
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import normalize

# Get data
dat = pd.read_csv('./Site281WS.txt', sep=r'\s+', index_col=False)
dat.Index = dat.Index.map(pd.to_datetime)
dat['day'] = dat.Index.map(lambda x: x.day)
dat['month'] = dat.Index.map(lambda x: x.month)
dat['year'] = dat.Index.map(lambda x: x.year)
dat.drop('Index',axis=1,inplace=True)
dat.drop('time',axis=1,inplace=True)

dat['mean50m'] = pd.DataFrame({'a':dat.ix[:,0].values, 'b':dat.ix[:,1].values}).max(1)
dat['mean80m'] = pd.DataFrame({'a':dat.ix[:,2].values, 'b':dat.ix[:,3].values}).max(1)

parsed_dat = pd.DataFrame({'day':[], 'month':[], 'year':[], 'mean50m':[], 'mean80m':[]})
for year in np.unique(dat.year)[0:-1]:
    for month in np.unique(dat.month): 
        for day in np.unique(dat.day):
            subdat = dat[ np.logical_and(dat.day==day, np.logical_and(dat.year==year, dat.month==month))]
            w50m = np.nanmean(subdat['mean50m'])
            w80m = np.nanmean(subdat['mean80m'])
            parsed_dat = parsed_dat.append({'day':day, 'month':month, 'year':year, 'mean50m':w50m, 'mean80m':w80m}, ignore_index=True)

# Parse reference data
refdat = pd.read_csv('710543.csv', index_col = False)
refdat.DATE = [pd.to_datetime(str(s)) for s in refdat.DATE]
refdat["year"] = refdat.DATE.map(lambda x: x.year)
refdat["month"] = refdat.DATE.map(lambda x: x.month)
refdat["day"] = refdat.DATE.map(lambda x: x.day)
for badfield in ["DATE", "STATION", "STATION_NAME", "ELEVATION", "LATITUDE", "LONGITUDE"]:
    refdat.drop(badfield, axis=1, inplace=True)
refdat = refdat.convert_objects(convert_numeric=True)

# wind data starts 1998
refdat = refdat[refdat.year > 1998]

# Ignore flags, take to ne data frame
rdf = pd.DataFrame()
rdf["year"] = refdat["year"]
rdf["month"] = refdat["month"]
rdf["day"] = refdat["day"]
for i in range(len(refdat.columns)/5):
    vals = refdat[refdat.columns[i*5]]
    vals[vals==np.nan] = np.nan
    vals[vals==9999] = np.nan
    rdf[ refdat.columns[i*5]] = vals

# NA Columns
rdf.drop(['ACMH', 'ACSH', 'TSUN', 'WT22'], axis=1, inplace=True)

# NA sanitization
for col in rdf.columns:
    rdf[col][ rdf[col] == -9999] = np.nan
    rdf[col][ rdf[col] == 9999] = np.nan
    rdf[col][ np.isnan( rdf[col].values)] = np.nan

# Merge nrel data with reference data
train = pd.merge( parsed_dat, rdf, how="left", on = ["year", "month", "day"])
#test = rdf
test = pd.merge( parsed_dat, rdf, how="right", on = ["year", "month", "day"])

train = train[np.logical_not(np.isnan(train.mean50m))]
train = train[np.logical_not(np.isnan(train.mean80m))]
train = train.reset_index(drop=True)

# take sample
rows = np.random.choice(train.index.values, 500)
train_known = train.ix[train.index[rows]]
train.drop(rows, inplace = True)

# train
target1 = u'mean50m'
target2 = u'mean80m'
Dtrain1 = xgb.DMatrix(train.drop('day',axis=1).drop('month',axis=1).drop('year',axis=1).drop(target1,axis=1).drop(target2,axis=1), train[target1], missing=np.nan)
Dtrain2 = xgb.DMatrix(train.drop('day',axis=1).drop('month',axis=1).drop('year',axis=1).drop(target1,axis=1).drop(target2,axis=1), train[target2], missing=np.nan)
Dtest = xgb.DMatrix(train_known.drop('day',axis=1).drop('month',axis=1).drop('year',axis=1).drop(target1,axis=1).drop(target2,axis=1), missing=np.nan)
modl1 = xgb.train({'subsample':0.7},Dtrain1, 6 )# 600)
modl2 = xgb.train({'subsample':0.7},Dtrain2, 6 )# 600)
pred1 = modl1.predict(Dtest)
pred2 = modl2.predict(Dtest)
print 'err1 is ',mean_squared_error(train_known[target1], pred1)
print 'err2 is ',mean_squared_error(train_known[target2], pred1)

# Predict
DT = xgb.DMatrix( test.drop([target1, target2, 'day'] ,axis=1).drop('month',axis=1).drop('year',axis=1), missing=np.nan)
test['pred'+target1] = modl1.predict(DT)
test['pred'+target2] = modl2.predict(DT)

# check distributions
plt.hist(test['pred'+target1])
plt.savefig("finalHist"+target1+".pdf")
plt.close()

target1 = 'predmean50m'
target2 = 'predmean80m'
# annual averages
means = {}
means[target1] = []
means[target2] = []
shears = []
for year in np.unique( test.year):
    strain = test[test.year==year]
    means[target1].append( np.mean(strain[target1]))
    means[target2].append( np.mean(strain[target2]))
    shears.append( np.nanmean( np.log( np.ma.masked_invalid( strain[target2] / strain[target1])) / np.log(80./50)))

# plot shear coefficients
sns.distplot(shears)
mu, std = norm.fit(shears)
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2, label='normal distribution')
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)
plt.savefig('shear_pdf.pdf')
plt.clf()

# plot average speeds
sns.distplot(means[target1])
mu, std = norm.fit(means[target1])
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2, label='normal distribution')
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)
plt.savefig('avg_speed_at_50m.pdf')
plt.clf()

sns.distplot(means[target2])
mu, std = norm.fit(means[target2])
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2, label='normal distribution')
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)
plt.savefig('avg_speed_at_80_m.pdf')
plt.close()
