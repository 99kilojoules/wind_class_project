import windrose
import pandas as pd
from scipy import stats
from scipy.optimize import minimize
import seaborn as sns
import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import mean_squared_error

# Load data
dat_p1 = pd.read_excel('./WapakonetaData.xls')
dat_p2 = pd.read_excel('./WapakonetaData.xls', 1)
for data in [dat_p1, dat_p2]:
    data[u'Date & Time Stamp'] = pd.to_datetime(data[u'Date & Time Stamp'], "Y-m-d H:M:S")
dat = pd.merge(dat_p1, dat_p2, on = u'Date & Time Stamp')
dat.set_index([u'Date & Time Stamp'],inplace=True)

# Pole shading
heights = [40, 71, 95] # m
heights_180 = [dat[x] for x in [u'CH1Avg', u'CH4Avg', u'CH5Avg']]
heights_360 = [dat[x] for x in [u'CH2Avg', u'CH3Avg', u'CH6Avg']]
vars_180 = [dat[x] for x in [u'CH1SD', u'CH4SD', u'CH5SD']]
vars_360 = [dat[x] for x in [u'CH2SD', u'CH3SD', u'CH6SD']]
vains = [dat[x] for x in [u'CH7Avg', u'CH8Avg', u'CH9Avg']]
for i in range(3):
    speeds = []
    devs = []
    for speed1, speed2, wind_dir, var1, var2 in zip(heights_180[i], heights_360[i], vains[i], vars_180[i], vars_360[i]):
        if var1 < 0.0001 and var2 < 0.0001:
            speeds.append(np.nan)
            devs.append(np.nan)
            continue
        if wind_dir > 90 or wind_dir < 270:
            speeds.append(speed1)
            devs.append(var1)
        else: 
            speeds.append(speed2)
            devs.append(var2)
    dat['avg_speed_at_'+str(heights[i])+'_m'] = speeds
    dat['avg_var_at_'+str(heights[i])+'_m'] = devs

# Go back and clean data:
   # from boom # 5 in  March 2006 - Data corruption predates March 14, 2006
   # from boom # 6 November 13-15, 2005
   # (Assuming the larger speed is more accurate during periods of instrument damage)
inds =  np.logical_or( np.logical_and(np.logical_and(dat.index.map(lambda x: x.month) ==3, dat.index.map(lambda x: x.year) == 2006), dat.index.map(lambda x: x.day) < 28), np.logical_and(dat.index.map(lambda x: x.year) == 2005, np.logical_and(dat.index.map(lambda x: x.month) ==11, np.logical_and(dat.index.map(lambda x: x.day)>12, dat.index.map(lambda x: x.day) < 15))))
for i in range(3):
    dat['avg_speed_at_'+str(heights[i])+'_m'][inds] =  pd.DataFrame({1:heights_180[i], 2:heights_360[i]}).max(1)
    dat['avg_var_at_'+str(heights[i])+'_m'][inds] = pd.DataFrame({1:vars_180[i], 2:vars_360[i]}).max(1)

# Wind rose
wind_dat_71m_1yr = pd.DataFrame({'speed': dat[u'avg_speed_at_71_m'], 'direction': vains[1]}) #[dat[u'Date & Time Stamp'].map(lambda x: x.year) == 2006]
windrose.plot_windrose(wind_dat_71m_1yr, kind='bar')
plt.savefig('wind_rose_1yr_71m.pdf')
plt.close()

# Histogram and Weibull fit
plt.figure(figsize=(20,10))
speeds = dat[u'avg_speed_at_71_m']
shape, _, scale = stats.weibull_min.fit([s for s in speeds if not np.isnan(s)],loc=0)
plt.hist(speeds, bins=np.linspace(0, 35, 70), normed=True, color="blue", label="Actual Data", alpha=0.5)
plt.scatter(speeds, stats.weibull_min.pdf(speeds, *stats.weibull_min.fit([s for s in speeds if not np.isnan(s)],loc=0)), c="red", label="Weibull Fit")
plt.legend()
plt.xlabel('Wind Speed at 71m (m/s)')
plt.ylabel('frequency')
plt.title('scale = '+str(np.round(scale,1))+', shape = '+str(np.round(shape,1)))
plt.savefig("weibs.pdf")
plt.close()

# plot monthly mean wind speed for each month for each height
plt.figure(figsize=(20,10))
for i in range(3):
    speeds = { }
    for month in range(1,13):
        speeds[month] = np.mean( dat['avg_speed_at_'+str(heights[i])+'_m'][dat.index.map(lambda x: x.month) == month])
    plt.xticks(np.arange(1, 13, 1.0))
    plt.plot(speeds.keys(),speeds.values(), linewidth = 5, label = str(heights[i])+"m Elevation")
    plt.xlabel('Month')
    plt.ylabel('Mean Wind Speed (m/s)')
plt.legend()
plt.savefig("Month_Mean_Speeds.pdf")
plt.close()

# Calculate shear coefficients
plt.figure(figsize=(20,10))
for i in range(2):
    shears = {}
    for month in range(1,13):
        subdat = dat[dat.index.map(lambda x: x.month) == month]
        shears[month] = np.mean ( np.log( subdat['avg_speed_at_'+str(heights[i+1])+'_m'] / subdat['avg_speed_at_'+str(heights[i])+'_m']) / np.log(heights[i+1] / float(heights[i])))
    plt.xticks(np.arange(1, 13, 1.0))
    plt.plot(shears.keys(), shears.values(), linewidth = 5, label = str(heights[i])+"-"+str(heights[i+1])+'m Elevation')
plt.xlabel('Month')
plt.ylabel("Mean Shear Coefficient")
plt.legend(loc = 4, prop={'size':14})
plt.savefig('shear_plot.pdf')
plt.close()

# check if the result makes sense by plotting scatter of wind speeds
plt.figure(figsize=(20,10))
cols = ['red', 'green', 'blue']
marks = ['*','^','o']
for i in range(3):
    plt.scatter(dat.index, dat['avg_speed_at_'+str(heights[i])+'_m'], c=cols[i], linewidth=0, label=heights[i], alpha = 0.5)
plt.legend(loc = 4, prop={'size':14})
plt.savefig('scatter_speeds.pdf')
plt.close()

# Calculate turbulance intensities
plt.figure(figsize=(20,10))
for i in range(3):
    Ts = {}
    for month in range(1,13):
        subdat = dat[dat.index.map(lambda x: x.month) == month]
        Ts[month] = np.mean( subdat['avg_var_at_'+str(heights[i])+'_m'] / subdat['avg_speed_at_'+str(heights[i])+'_m'] )
    plt.xticks(np.arange(1, 13, 1.0))
    plt.plot(Ts.keys(), Ts.values(), linewidth = 5, label = str(heights[i])+' m')
plt.legend(loc = 4, prop={'size':14})
plt.xlabel('Month')
plt.title("Turbulance Intensity")
plt.savefig('turbulance_intensity.pdf')
plt.close()

# Calculate theoretical air densities
plt.figure(figsize=(20,10))
h = 271 # m above sea level
for i in range(3):
    roes = {}
    for month in range(1,13):
        subdat = dat[dat.index.map(lambda x: x.month) == month]
        roes[month] = np.mean((353.05/(273+subdat[ u'CH10Avg'])) * np.exp(-0.034 * (h + heights[i])/(273+subdat[ u'CH10Avg'])))
    plt.xticks(np.arange(1, 13, 1.0))
    plt.plot(roes.keys(), roes.values(), linewidth = 5, label = str(heights[i])+' m')
plt.legend()
plt.savefig('density.pdf')
plt.close()

# Calculate year-month-day average wind speeds
days  = np.unique(dat.index.map(lambda x: x.day))
months = np.unique(dat.index.map(lambda x: x.month))
years = np.unique(dat.index.map(lambda x: x.year))
frame = pd.DataFrame({'average site speed':[], 'height':[], 'year':[], 'month':[], 'day':[]})
for i in range(3):
    for month in months:
        for day in days:
            for year in years:
                 subdat = dat[np.logical_and( dat.index.map(lambda x: x.day) == day, np.logical_and( dat.index.map(lambda x: x.month) == month, dat.index.map(lambda x: x.year) == year))]
                 avg = np.mean(subdat['avg_speed_at_'+str(heights[i])+'_m'])
                 if not np.isnan(avg): frame = frame.append({'month': month, 'year': year, 'height': heights[i],'day': day, 'average site speed': avg}, ignore_index=True)

# Get reference data
oardc = pd.read_excel('OARDCdata.xlsx')
oardc[u'Date'] = pd.to_datetime(oardc[u'Date'], "Y-m-d")
oardc['day'] = oardc[u'Date'].map(lambda x: x.day)
oardc['month'] = oardc[u'Date'].map(lambda x: x.month)
oardc['year'] = oardc[u'Date'].map(lambda x: x.year)

# assuming site speed = a * (reference speed) + b, solve for a and b
train = pd.merge(frame, oardc, how="left", on = ["year", "month", "day"])
def sim(X, height): # X = [a,b]
    subtrain = train[train['height']==height]
    return mean_squared_error(subtrain[u'average site speed'].values, X[0] *subtrain[u'Avg. Wind Speed'].values + X[1])

linear_coefs = [] # [ [a1,b1,h1,mse], ..
for i in range(3):
    mins = minimize(sim, [1, 1], args =( np.unique(train.height)[i]))
    linear_coefs.append(mins.x.tolist()+[mins.fun])

# scatter plot comparing site data to reference data
test = pd.merge(frame, oardc, how="right", on = ["year", "month", "day"])
plt.figure(figsize=(20,10))
cols = ['red', 'blue', 'green']
for i in range(3):
    subdat = test[test['height']==heights[i]]
    plt.scatter(subdat[ u'Avg. Wind Speed'], subdat[u'average site speed'], s=100, c=cols[i], alpha = 0.4, label = str(heights[i])+' m')
    plt.plot(subdat[ u'Avg. Wind Speed'], subdat[ u'Avg. Wind Speed'] * linear_coefs[i][0] + linear_coefs[i][1], c=cols[i], label = 'trendline for '+str(heights[i])+' m')
plt.xlabel('Site Wind Speed (m/s)')
plt.ylabel('Reference Wind Speed (m/s)')
plt.legend(loc="upper left")
plt.savefig('speed_site_ref_correlation.pdf')
plt.close()

# Long-term forecast
for i in range(3):
    test['linear forecast for '+str(heights[i]) + ' m'] = test[ u'Avg. Wind Speed'].convert_objects(convert_numeric=True) * linear_coefs[i][0] + linear_coefs[i][1]

# Weibull plot/fit for long-term forecasts
def plot_weibs(filename, speeds):
    plt.figure(figsize=(20,10))
    shape, _, scale = stats.weibull_min.fit( [s for s in speeds if not np.isnan(s)],loc=0)
    plt.hist(speeds, bins=np.linspace(0, 35, 70), normed=True, color="blue", label="Actual Data", alpha=0.5)
    plt.scatter(speeds, stats.weibull_min.pdf(speeds, *stats.weibull_min.fit( [s for s in speeds if not np.isnan(s)],loc=0)), c="red", label="Weibull Fit")
    plt.legend()
    plt.xlabel('Wind Speed at '+str(filename)+'m (m/s)')
    plt.ylabel('frequency')
    plt.title('scale = '+str(np.round(scale,1))+', shape = '+str(np.round(shape,1)))
    plt.savefig(filename+".pdf")
    plt.close()

speeds = test['linear forecast for '+str(heights[0]) + ' m']
plot_weibs('80m_forecast', speeds * (80. / heights[0]) ** 0.33)
plot_weibs('95m_forecast', speeds * (95. / heights[0]) ** 0.33)

# find annual mean shear coefficient and wind speed
coefs = []
avgs = []
for year in np.unique(test[u'year']):
    subtest = test[test[u'year']==year]
    avgs.append(np.mean(subtest[u'linear forecast for 40 m']))
    coefs.append(np.mean( np.log( subtest[u'linear forecast for 95 m'] / subtest[u'linear forecast for 40 m']) / np.log(95./45)))

# plot shear coefficients
sns.distplot(coefs)
mu, std = norm.fit(coefs)
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2, label='normal distribution')
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)
plt.savefig('shear_pdf.pdf')

# plot average speeds
sns.distplot(avgs)
mu, std = norm.fit(avgs)
xmin, xmax = plt.xlim()
x = np.linspace(xmin, xmax, 100)
p = norm.pdf(x, mu, std)
plt.plot(x, p, 'k', linewidth=2, label='normal distribution')
title = "Fit results: mu = %.2f,  std = %.2f" % (mu, std)
plt.title(title)
plt.savefig('avg_pdf.pdf')
